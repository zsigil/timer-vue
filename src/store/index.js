import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    counting: false,
    paused: false,
    counter: 0,
  },
  getters: {
    counting: state => state.counting,
    counter: state => state.counter,
    paused: state => state.paused,
  },
  mutations: {
    setCounter(state, payload){
      state.counter = payload;
    },
    paused(state, payload){
      state.paused = payload;
    },
    counting(state, payload){
      state.counting = payload;
    },

  },
  actions: {
    setCounter({commit}, payload){
      commit('setCounter', payload)
    },
    counting({commit}, payload){
      commit('counting', payload)
    },
    paused({commit}, payload){
      commit('paused', payload)
    },

  },
  modules: {
  }
})
